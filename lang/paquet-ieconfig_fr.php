<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/ieconfig.git

return [

	// I
	'ieconfig_description' => 'Ce plugin permet d’exporter la configuration de SPIP et des plugins compatibles dans un seul et unique fichier au format YAML. Ces fichiers de configuration peuvent ensuite être importés.',
	'ieconfig_slogan' => 'Importer, exporter des configurations de plugins',
];
